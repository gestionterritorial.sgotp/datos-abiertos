<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ServicioMapa extends Model
{
    use HasFactory;

    use Sluggable;

    protected $guarded = [];
    protected $table = 't_servicio_mapa';


    public function plan(){
        return $this->belongsTo(Plan::class);

    }


    public function componente(): HasOne{
        return $this->hasOne(Componente::class, 'id', 'componente_id');
    }

    public function Sluggable():array{
        return [
            'slug'=> [
                'source'=> 'nombre'
            ]
            ];
    }
}
