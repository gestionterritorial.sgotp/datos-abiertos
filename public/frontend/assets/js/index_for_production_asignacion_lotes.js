/*


import MVT from 'ol/format/MVT.js';
import Map from 'ol/Map.js';
import OSM from 'ol/source/OSM.js';
import TileLayer from 'ol/layer/Tile.js';
import VectorTileLayer from 'ol/layer/VectorTile.js';
import VectorTileSource from 'ol/source/VectorTile.js';
import View from 'ol/View.js';
import {Fill, Stroke, Style, Text} from 'ol/style.js';
import {fromLonLat} from 'ol/proj';


*/


///----------------------------------------

var bbox;

// fetch(`https://apicolector.municusco.com/v1/bbox/asignacion/public.ficha_lote/${personal_id}?geom_column=geom&srid=4326`)
 
        
// .then(res => res.json())
// .then((out) => {

//     let obj = JSON.parse(out[0].bbox);
//      console.log(JSON.stringify(obj.coordinates[0]));
//     // map.fitBounds( obj.coordinates[0]);
//     bbox=obj.coordinates[0];

// }).catch(err => console.error(err));
///----------------------------------------

function createList(data) {

  let val = `<ul class="ul-vertical">`;

  let length = Object.keys(data).length;


 for (let clave in data){
  val+= `<li><strong>Lote: </strong>${data[clave].properties_.lote}</li>`;
 }
  


// return `${data[fid].properties_.lote}`;
// return `${length}`;
return `${val}</ul>`;
}

const info = document.getElementById('info');

//inicializar los contenedores
/**
 * Elements that make up the popup.
 */
const container = document.getElementById('popup');
const content = document.getElementById('popup-content');
const closer = document.getElementById('popup-closer');


//inicializar el Overlay
/**
 * Create an overlay to anchor the popup to the map.
 */

const overlay = new ol.Overlay({
  element: container,
  autoPan: {
    animation: {
      duration: 250,
    },
  },
});


/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function () {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};


const view = new ol.View({
  center: ol.proj.fromLonLat([-71.9774868, -13.5169427]),
  zoom: 13,
  multiWorld: true,
});


// lookup for selection objects
let selection = {};

const labelLoteStyle = new ol.style.Style({
  text: new ol.style.Text({
    font: "13px Calibri,sans-serif",
    fill: new ol.style.Fill({
      color: "#000",
    }),
    stroke: new ol.style.Stroke({
      color: "#fff",
      width: 3,
    }),
  }),
  stroke: new ol.style.Stroke({
    color: "#000",
    width: 0.5,
  }),
  fill: new ol.style.Fill({
    color: "rgba(133, 131, 131, 0.0)",
  }),
});

const labelLoteFiltradoStyle = new ol.style.Style({
  text: new ol.style.Text({
    font: "13px Calibri,sans-serif",
    fill: new ol.style.Fill({
      color: "#000",
    }),
    stroke: new ol.style.Stroke({
      color: "#fff",
      width: 3,
    }),
  }),
  stroke: new ol.style.Stroke({
    color: "#000",
    width: 1.5,
  }),
  fill: new ol.style.Fill({
    color: "rgba(94, 255, 0, 1)",
  }),
});


const labelManzanaStyle = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: "black",
    width: 2.5,
  }),
  fill: new ol.style.Fill({
    color: "rgba(133, 131, 131, 0.0)",
  }),
  text: new ol.style.Text({
    font: "15px Calibri,sans-serif",
    fill: new ol.style.Fill({
      color: "#000",
    }),
    stroke: new ol.style.Stroke({
      color: "#fff",
      width: 3,
    }),
  }),
});

const country = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: "gray",
    width: 1,
  }),
  fill: new ol.style.Fill({
    color: "rgba(133, 131, 131, 0.46)",
  }),
});

const manzana = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: "black",
    width: 1,
  }),
  fill: new ol.style.Fill({
    color: "rgba(133, 131, 131, 0.0)",
  }),
});


var styleFunctionLote = function (feature) {
    return new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: "rgba(56, 56, 56, 0.96)",
        width: 2,
      }),
      text: new ol.style.Text({
        text: `${feature.get("id")}`, // Use a property from your feature for labeling
        font: "bold 17px Calibri,sans-serif",
        fill: new ol.style.Fill({
          color: "black",
        }),
        stroke: new ol.style.Stroke({
          color: "white",
          width: 2,
        }),
      }),
    });
  };

var styleFunctionManzana = function (feature) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "rgba(56, 56, 56, 0.96)",
      width: 2,
    }),
    text: new ol.style.Text({
      text: `${feature.get("codigo")}`, // Use a property from your feature for labeling
      font: "bold 17px Calibri,sans-serif",
      fill: new ol.style.Fill({
        color: "black",
      }),
      stroke: new ol.style.Stroke({
        color: "white",
        width: 2,
      }),
    }),
  });
};

var styleFunctionZonaOpera = function (feature) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "rgb(255, 0, 0)",
      width: 2,
    }),
    text: new ol.style.Text({
      text: `${feature.get("zona_operacional")}`, // Use a property from your feature for labeling
      font: "bold 17px Calibri,sans-serif",
      fill: new ol.style.Fill({
        color: "rgb(255, 0, 0)",
      }),
      stroke: new ol.style.Stroke({
        color: "white",
        width: 2,
      }),
    }),
  });
};

var styleFunctionSubArea = function (feature) {
  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "rgb(255, 217, 0)",
      width: 3.0,
      lineDash: [3, 5],
    }),
    text: new ol.style.Text({
      text: `${feature.get("nombre")}`, // Use a property from your feature for labeling
      font: "bold 19px Calibri,sans-serif",
      fill: new ol.style.Fill({
        color: "rgb(255, 217, 0)",
      }),
      stroke: new ol.style.Stroke({
        color: "rgb(53, 53, 53)",
        width: 2,
      }),
    }),
  });
};

const style = [country, labelLoteStyle];

const selectedCountry = new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: "rgb(255, 0, 0)",
    // color: 'rgba(60, 255, 0,0.8))',
    width: 2.5,
  }),
  fill: new ol.style.Fill({
    color: "rgb(255, 242, 0)",
    // color: 'rgba(60, 255, 0,0.8))',
  }),
});

const vtLayer = new ol.layer.VectorTile({
    title: "Lotes",
  declutter: true,
  source: new ol.source.VectorTile({
    maxZoom: 15,
    format: new ol.format.MVT({
      idProperty: "id",
    }),
    url:
      // 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
      // 'ne:ne_10m_admin_0_countries@EPSG:900913@pbf/{z}/{x}/{-y}.pbf',
      // 'http://localhost:8080/geoserver/gwc/service/tms/1.0.0/sgotp-41zre%3Atgeo_lote@EPSG:900913@pbf/{z}/{x}/{y}.pbf'
      // 'https://api.cusco.cloud/v1/mvt/public.division_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id',
      "https://apicolector.municusco.com/v1/mvt/public.ficha_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id,zona_id,lote,codigo",
  }),
  //style: country,
  style: function (feature) {
    labelLoteStyle

      .getText()
      .setText([
        // feature.getId(),
        // "bold 13px Calibri,sans-serif",
        ` ${feature.get("lote")}`,
        // "",
        // "\n",
        // "",
        //`Zona ID: ${feature.get("zona_id")}`,
        "italic 11px Calibri,sans-serif",
      ]);
    return style;
  },
});

const rasterOrtofotoPoroy = new ol.layer.Tile({
  title: "Ortofoto Poroy",
  visible: false,
  declutter: true,
  source: new ol.source.TileWMS({
    url: "https://geoserver.municusco.com/geoserver/colector_fichas/wms",
    params: {
      LAYERS: "colector_fichas:ORTOIMAGEN_POROY_5CM_4",
      TILED: true,
      "exceptions": 'application/vnd.ogc.se_inimage',
      //STYLES: "pdu_zonificacion_full",
      //'cql_filter': "codigo_zre ='" + this.codigo+ "'"
      // FORMAT_OPTIONS: "antialias:none",
    },
    ratio: 1,
    serverType: "geoserver",
  }),
  
});


const vtLayerFiltrado = new ol.layer.VectorTile({
  title: "Lotes Asignados",
declutter: true,
source: new ol.source.VectorTile({
  maxZoom: 15,
  format: new ol.format.MVT({
    idProperty: "id",
  }),
  url:
    // 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
    // 'ne:ne_10m_admin_0_countries@EPSG:900913@pbf/{z}/{x}/{-y}.pbf',
    // 'http://localhost:8080/geoserver/gwc/service/tms/1.0.0/sgotp-41zre%3Atgeo_lote@EPSG:900913@pbf/{z}/{x}/{y}.pbf'
    // 'https://api.cusco.cloud/v1/mvt/public.division_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id',
    // `http://localhost:3030/v1/mvt/asigna/public.ficha_lote/{z}/{x}/{y}/${personal_id}?geom_column=geom&columns=id&id_column=id`,
     `https://apicolector.municusco.com/apicolector/mvt/asigna/public.ficha_lote/{z}/{x}/{y}/${personal_id}?geom_column=geom&columns=id,asignacion_id,lote_id,personal_id&id_column=id`,
}),
//style: country,
style: function (feature) {
  labelLoteFiltradoStyle

    .getText()
    .setText([
      // feature.getId(),
      // "bold 13px Calibri,sans-serif",
      ` ${feature.get("lote")}`,
      // "",
      // "\n",
      // "",
      //`Zona ID: ${feature.get("zona_id")}`,
      "italic 11px Calibri,sans-serif",
    ]);
  //return style;
},
});

vtLayerFiltrado.setStyle(labelLoteFiltradoStyle);



const vtLayerManzana = new ol.layer.VectorTile({
  title: "Manzanas",
  declutter: true,
  source: new ol.source.VectorTile({
    maxZoom: 15,
    format: new ol.format.MVT({
      idProperty: "id",
    }),
    url:
      // 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
      // 'ne:ne_10m_admin_0_countries@EPSG:900913@pbf/{z}/{x}/{-y}.pbf',
      // 'http://localhost:8080/geoserver/gwc/service/tms/1.0.0/sgotp-41zre%3Atgeo_lote@EPSG:900913@pbf/{z}/{x}/{y}.pbf'
      // 'https://api.cusco.cloud/v1/mvt/public.division_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id',
      "https://apicolector.municusco.com/v1/mvt/public.configuracion_manzana/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id,codigo",
  }),
  //style: manzana,
  /*style:function (feature) {
    labelManzanaStyle
      
      .getText()
      .setText([
        feature.getId(),
        'bold 13px Calibri,sans-serif',
        ` ${feature.get('name')}`,
        '',
        '\n',
        '',
        `Zona ID: ${feature.get('codigo')}`,
        'italic 11px Calibri,sans-serif',
      ]);
      
    return style;
  }*/
});

vtLayerManzana.setStyle(styleFunctionManzana);


const vtLayerZonaOpera = new ol.layer.VectorTile({
  title: "Zonas Oper.",
  declutter: true,
  source: new ol.source.VectorTile({
    maxZoom: 15,
    format: new ol.format.MVT({
      idProperty: "id",
    }),
    url:
      // 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
      // 'ne:ne_10m_admin_0_countries@EPSG:900913@pbf/{z}/{x}/{-y}.pbf',
      // 'http://localhost:8080/geoserver/gwc/service/tms/1.0.0/sgotp-41zre%3Atgeo_lote@EPSG:900913@pbf/{z}/{x}/{y}.pbf'
      // 'https://api.cusco.cloud/v1/mvt/public.division_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id',
      "https://apicolector.municusco.com/v1/mvt/public.configuracion_zona/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id,zona_operacional",
  }),
  //style: manzana,
  /*style:function (feature) {
    labelManzanaStyle
    
    .getText()
    .setText([
      feature.getId(),
      'bold 13px Calibri,sans-serif',
        ` ${feature.get('name')}`,
        '',
        '\n',
        '',
        `Zona ID: ${feature.get('codigo')}`,
        'italic 11px Calibri,sans-serif',
      ]);
      
      return style;
    }*/
});

vtLayerZonaOpera.setStyle(styleFunctionZonaOpera);


//SUB ZONAS OPERACIONALES--------------------------------------------------------------------

const vtLayerSubArea = new ol.layer.VectorTile({
    title: "Sub Areas",  
  declutter: true,
  source: new ol.source.VectorTile({
    maxZoom: 15,
    format: new ol.format.MVT({
      idProperty: "id",
    }),
    url:
      // 'https://ahocevar.com/geoserver/gwc/service/tms/1.0.0/' +
      // 'ne:ne_10m_admin_0_countries@EPSG:900913@pbf/{z}/{x}/{-y}.pbf',
      // 'http://localhost:8080/geoserver/gwc/service/tms/1.0.0/sgotp-41zre%3Atgeo_lote@EPSG:900913@pbf/{z}/{x}/{y}.pbf'
      // 'https://api.cusco.cloud/v1/mvt/public.division_lote/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id',
      "https://apicolector.municusco.com/v1/mvt/public.configuracion_subarea/{z}/{x}/{y}?geom_column=geom&precision=9&columns=id,nombre",
  }),
  //style: manzana,
  /*style:function (feature) {
      labelManzanaStyle
      
      .getText()
      .setText([
        feature.getId(),
        'bold 13px Calibri,sans-serif',
          ` ${feature.get('name')}`,
          '',
          '\n',
          '',
          `Zona ID: ${feature.get('codigo')}`,
          'italic 11px Calibri,sans-serif',
        ]);
        
        return style;
      }*/
});

vtLayerSubArea.setStyle(styleFunctionSubArea);




//map.addLayer();

const map = new ol.Map({

  

  overlays: [overlay],
  layers: [
    // new ol.layer.Tile({
    //   source: new ol.source.OSM(),
    // }),
    baseLayers,
    rasterOrtofotoPoroy,
    vtLayer,
    vtLayerFiltrado,
    vtLayerManzana,
    vtLayerZonaOpera,
    vtLayerSubArea,
    //brgm,
    //labels,
    //vectorJson
  ],

  target: "map",
  view: view,
});

// Selection
const selectionLayer = new ol.layer.VectorTile({
  map: map,
  renderMode: "vector",
  source: vtLayer.getSource(),
  style: function (feature) {
    if (feature.getId() in selection) {
      return selectedCountry;
    }
  },
});

const selectElement = document.getElementById("type");

map.on(["click", "pointermove"], function (event) {



  if (
    (selectElement.value === "singleselect-hover" &&
      event.type !== "pointermove") ||
    (selectElement.value !== "singleselect-hover" &&
      event.type === "pointermove")
  ) {
    return;
  }


    
  vtLayer.getFeatures(event.pixel).then(function (features) {
    if (!features.length) {
      selection = {};
      selectionLayer.changed();
      info.innerText = '';
      return;
    }
    const feature = features[0];
    if (!feature) {
      return;
    }
    const fid = feature.getId();

    if (selectElement.value.startsWith("singleselect")) {
      selection = {};
    }

    
    //POP-UP-----------------------------------
    // const properties = features[0].getProperties();
    // info.innerText = JSON.stringify(properties, null, 2)
    //END POP-UP-----------------------------------

    // add selected feature to lookup
    selection[fid] = feature;

    
        //popup
        const coordinate = event.coordinate;
        const hdms = ol.coordinate.toStringHDMS(ol.proj.toLonLat(coordinate));
        console.log(selection[fid].properties_.lote);
        content.innerHTML = `
                            <!--<p> Hicijte Click aqui:</p>-->
                              Código Lote: <span style="font-weight: bold;"> ${selection[fid].properties_.lote} </span>
                              <br/> 
                               Código Lote Full: <span style="font-weight: bold;"><a href="#">${selection[fid].properties_.codigo} </a></span>
                              <br/> 
                            <!--  <code>${hdms} </code>-->
                            `;
        overlay.setPosition(coordinate);


    // info.innerText = JSON.stringify(selection, null, 2)
    info.innerHTML = createList(selection);

    selectionLayer.changed();

    console.log(selection);
  });
});

map.addControl(
  new ol.control.LayerSwitcher({
    oninfo: function (l) {
      $(".options").html(l.get("title") + "<br/>");
      switch (l.get("title")) {
        case "GEOLOGIE":
          $("<img>")
            .appendTo($(".options"))
            .attr(
              "src",
              "http://geoservices.brgm.fr/geologie?language=fre&version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&layer=GEOSERVICES_GEOLOGIE&format=image/png&STYLE=default"
            );
          break;
        default:
          break;
      }
    },
  }),
  
);



// // map.on('pointermove', showInfo);



// // function showInfo(event) {
// //   const features = vtLayer.getFeaturesAtPixel(event.pixel);
// //   if (features.length == 0) {
// //     info.innerText = '';
// //     info.style.opacity = 0;
// //     return;
// //   }
// //   const properties = features[0].getProperties();
// //   info.innerText = JSON.stringify(properties, null, 2);
// //   info.style.opacity = 1;
// // }

// map.on('singleclick', function (evt) {
//   document.getElementById('info').innerHTML = '';
//   const viewResolution = /** @type {number} */ (view.getResolution());
//   const url = vtLayer.getFeatureInfoUrl(
//     evt.coordinate,
//     viewResolution,
//     'EPSG:3857',
//     {'INFO_FORMAT': 'text/html'}
//   );
//   if (url) {
//     fetch(url)
//       .then((response) => response.text())
//       .then((html) => {
//         document.getElementById('info').innerHTML = html;
//       });
//   }
// });