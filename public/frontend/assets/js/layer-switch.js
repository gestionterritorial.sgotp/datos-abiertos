
  // A group layer for base layers
  var baseLayers = new ol.layer.Group({
    title: 'Base Layers',
    openInLayerSwitcher: false,
    layers: [
      new ol.layer.Tile({
        title: "Watercolor",
        baseLayer: true,
        visible:false,
        source: new ol.source.Stamen({
          layer: 'watercolor'
        })
      }),
      new ol.layer.Tile({
        title: "Toner",
        baseLayer: true,
        visible: false,
        source: new ol.source.Stamen({
          layer: 'toner'
        })
      }),
      new ol.layer.Tile({
        title: "Google Map Hybrid",
        baseLayer: true,
        visible: false,
        // Again set this layer as a base layer
        //visible: true,
        source: new ol.source.XYZ({
          url: "http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}",
        }),
      }),
      new ol.layer.Tile({
        title: "OSM",
        baseLayer: true,
        source: new ol.source.OSM(),
        visible: true
      })
    ]
  });

  // An overlay that stay on top
  var labels = new ol.layer.Tile({
    title: "Labels (on top)",
    allwaysOnTop: true,			// Stay on top of layer switcher
    noSwitcherDelete: true,		// Prevent deleting from layer switcher
    source: new ol.source.Stamen({ layer: 'terrain-labels' })
  });
  // WMS with bbox
  var brgm = new ol.layer.Tile ({
    "title": "GEOLOGIE",
    "extent": [
      -653182.6969582437,
      5037463.842847037,
      1233297.5065495989,
      6646432.677299531
    ],
    "minResolution": 3.527777777777778,
    "maxResolution": 3527.777777777778,
    "source": new ol.source.TileWMS({
      "url": "https://geoservices.brgm.fr/geologie",
      "projection": "EPSG:3857",
      "params": {
        "LAYERS": "GEOLOGIE",
        "FORMAT": "image/png",
        "VERSION": "1.3.0"
      },
      "attributions": [
        "<a href='http://www.brgm.fr/'>&copy; Brgm</a>"
      ]
    })
  });