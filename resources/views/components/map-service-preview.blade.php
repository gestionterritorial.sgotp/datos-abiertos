

        <div x-data="map()">
            // Use the x-ref to reference our map, this way, we can have multiple component on the page if needed
                <div x-ref="map" class="map h-[600px]">
                </div>
            </div>



@once
    @push('styles')
        @vite(['resources/css/components/map-preview-service.css'])
    @endpush
    @push('scripts')
        @vite(['resources/js/components/map-preview-service.js'])
    @endpush
@endonce
