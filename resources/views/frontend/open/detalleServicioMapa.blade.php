@extends('frontend.layout')

@section('header-style-scripts')
{{-- <link rel="stylesheet" href="/node_modules/ol/ol.css">
<style>
    .map {
      width: 100%;
      height: 400px;
    }
  </style>
  <script type="text/javascript" href="/node_modules/ol/ol.js"> --}}

    <script src="https://cdn.jsdelivr.net/npm/ol@v7.5.0/dist/ol.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <!-- Pointer events polyfill for old browsers, see https://caniuse.com/#feat=pointer -->
    <script src="https://cdn.jsdelivr.net/npm/elm-pep@1.0.6/dist/elm-pep.js"></script>
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/ol@v7.5.0/ol.css"
    />
    <link rel="stylesheet" href="{{ url ('frontend/assets/css/ol-ext.css') }}" />
    <script type="text/javascript" src="{{ url('frontend/assets/js/ol-ext.js') }}"></script>
    {{-- <link rel="stylesheet" href="{{ url('frontend/assets/css/style.css') }}" /> --}}
    <link rel="stylesheet" href="{{ url('frontend/assets/css/map.switcher2.css') }}" />

    <style>
      .map {
        width: 100%;
        height: 900px;
        /*position: absolute;*/
      }
      #info {
        z-index: 1;
        opacity: 1;
        position: absolute;
        bottom: 8px;
        left: 8px;
        margin: 0;
        background: rgba(0,60,136,0.25);
        color: white;
        border: 0;
        transition: opacity 100ms ease-in;
        overflow-x: hidden;
        overflow-y: auto;
        max-height: 300px;
        border-radius: 6px;
      }

      /*pop up*/
      .ol-popup {
        position: absolute;
        background-color: white;
        box-shadow: 0 1px 4px rgba(0,0,0,0.2);
        padding: 15px;
        border-radius: 10px;
        border: 1px solid #cccccc;
        bottom: 12px;
        left: -50px;
        min-width: 280px;
      }
      .ol-popup:after, .ol-popup:before {
        top: 100%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
      }
      .ol-popup:after {
        border-top-color: white;
        border-width: 10px;
        left: 48px;
        margin-left: -10px;
      }
      .ol-popup:before {
        border-top-color: #cccccc;
        border-width: 11px;
        left: 48px;
        margin-left: -11px;
      }
      .ol-popup-closer {
        text-decoration: none;
        position: absolute;
        top: 2px;
        right: 8px;
      }
      .ol-popup-closer:after {
        content: "✖";
      }
      /**/

      .ul-vertical {
        list-style: none;
        margin: 0;
        padding: 8px;
        background-color: rgba(177, 0, 97, 0.267);
        display: inline-block;
      }
      .ul-vertical li {
        background-color: rgba(255, 242, 0,0.8);
        font-size: 13px;
        width: 150px;
        height: 30px;
        margin: 0;
        padding: 2px;
        border: 1px solid rgb(255, 0, 0);
        color: #2e2e2e;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        border-radius: 6px;
      }
      .ul-vertical li+li {
        margin-top: 8px;
      }
    </style>
    <script src="{{ url('frontend/assets/js/layer-switch.js') }}"></script>
    <script>
        let params = (new URL(document.location)).searchParams;
        let personal_id = params.get("personal_id");
        var servicio;
    </script>
@endsection

@section('content')

<div class="main" style="padding-top: 6.5rem;">
    <div class="container" >
        <div class="row">
            <div class="col-md-12">
                <h1>

                    {{ $servicio->nombre }}
                </h1>

                <ol class="list-group list-group-numbered">
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                        <div class="fw-bold">Description</div>
                        {{ $servicio->descripcion}}
                      </div>
                      {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                        <div class="fw-bold">WMS</div>
                        {{ $servicio->wms }}
                      </div>
                      {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                        <div class="fw-bold">vmt</div>
                        {{ $servicio->vmt }}
                      </div>
                      {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                        <div class="fw-bold">geojson</div>
                        {{ $servicio->geojson }}
                      </div>
                      {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                      <div class="ms-2 me-auto">
                        <div class="fw-bold">Imagen</div>
                        {{ $servicio->url_image }}
                        <img src="/uploads/services_planes_images/{{ $servicio->url_image }}" class="img-fluid" style="max-height: 200px" alt="">
                      </div>
                      {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                          <div class="fw-bold">Descargas</div>
                          <a href="#" class="btn btn-light btn-sm"> <i class="ri-arrow-down-circle-line ri-lg"></i>&nbsp; geojson</a>
                          <a href="#" class="btn btn-light btn-sm"> <i class="ri-arrow-down-circle-line ri-lg"></i>&nbsp; kml</a>
                          <a href="#" class="btn btn-light btn-sm"> <i class="ri-arrow-down-circle-line ri-lg"></i>&nbsp; pdf</a>
                          <a href="#" class="btn btn-light btn-sm"> <i class="ri-arrow-down-circle-line ri-lg"></i>&nbsp; shape file</a>
                        </div>
                        {{-- <span class="badge bg-primary rounded-pill">1</span> --}}
                      </li>
                </ol>
                <ul class="list-group" >


                    <li class="list-group-item">{{ $servicio->url_image }}</li>
                    <li class="list-group-item">{{ $servicio->etapa_id }}</li>
                    <li class="list-group-item">{{ $servicio->componente_id }}</li>
                    <li class="list-group-item">{{ $servicio->url_layer_geoserver }}</li>


                </ul>

                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td>Description</td>
                        </tr>
                    </tbody>
                  </table>
            </div>

            <div class="col-md-12">
                <div id="map" class="map">

                    <pre id="info"/>
                </div>
                <div id="popup" class="ol-popup">
                  <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                  <div id="popup-content"></div>
                </div>
                <form>
                  <label for="type">Tipo de Selección &nbsp;</label>
                  <select id="type">
                    <option value="multiselect" selected>Multiple</option>
                    <option value="singleselect" >Simple</option>
                    <option value="singleselect-hover">Simple al superponer</option>
                  </select>
                  <select name="personal" id="personal">

                  </select>
                </form>

                            {{-- <x-map-service-preview/> --}}

            </div>
        </div>
    </div>

</div>

@endsection


@section('footer-style-scripts')

<script >

    // console.log('{{ $servicio->nombre }}');
    // console.log('{{ $servicio->url_layer_geoserver }}');
    servicio = @json($servicio);
    // var ss = {{  Js::from($servicio)  }};
    console.log(servicio.url_layer_geoserver);

</script>
<script

      src="{{ url('frontend/assets/js/index_for_production_consulta_avance_by_personal_by_fecha.js') }}"
    ></script>

@endsection
