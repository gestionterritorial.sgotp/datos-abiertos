<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ServiciosMapLivewire extends Component
{

    // public properties
    public $message ="Probando mi component";

    public $activo = true;

    public $counter = 0;


    public function render()
    {
        return view('livewire.servicios-map-livewire');
    }
    public function add(){
        return $this->counter++;
    }
}
